react-static-plugin-favicons
======
A plugin for [React Static](https://react-static.js.org/) that uses the [favicons](https://www.npmjs.com/package/favicons) module to generate favicons in many different sizes for many different platforms, given a source icon.

## Installation

```bash
npm install react-static-plugin-favicons --save-dev
```

## Usage

Add the plugin to your `static.config.js`, passing the path to your soure icon as an option:

```javascript
export default {
  plugins: [
    [
      'react-static-plugin-favicons',
      { inputFile: path.resolve(__dirname, 'icon.svg') },
    ],
  ]
};
```

## Options

Options [can be passed by using an array](https://github.com/nozzle/react-static/tree/master/docs/plugins#plugin-options).

### `inputFile: string`

Required.

Path to the icon that is to be your favicon.

### `outputDir: string`

Default value: `config.paths.assets`

Folder in which to output the generated icons. Defaults to [the same directory as the one used for bundled JS and CSS](https://github.com/nozzle/react-static/blob/master/docs/config.md#paths).

### `configuration: object`

Default value: `{}`

Options to be passed to the [favicons](https://www.npmjs.com/package/favicons) module. For a list of all possible configuration options, see https://www.npmjs.com/package/favicons#nodejs.

## Changelog

See [`CHANGELOG.md`](https://gitlab.com/Vinnl/react-static-plugin-favicons/blob/master/CHANGELOG.md).

## License

MIT © [Vincent Tunru](https://vincenttunru.com)
