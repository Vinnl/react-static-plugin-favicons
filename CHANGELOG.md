# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2019-08-19

### New features

- Compatibility with React Static v7 - thanks @maduraPradeep for the contribution!

### Breaking changes

- This version uses React Static's v7 API, and hence will no longer work with v6 or below.

## [1.0.0] - 2019-01-01

### New features

- First release!
